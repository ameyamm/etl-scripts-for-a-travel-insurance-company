import utils
'''Implement policy risk'''
class InsuredPartyLoader:
    '''
    Loads the insuredParty dimension
    '''

    def __init__(self,connection):
        '''
        Constructor
        '''
        self.__connection = connection
        
    def loadDimension(self):
        print "Loading Insured Party Dimension"
        self.__loadStaging()
        returnVal = self.__cleanupData()
        if not returnVal:
            self.__connection.rollback()
            return
        loadedRecords = self.__loadLookupAndDimension()
        #self.__loadLookup()
        #self.__loadDimensionTable()
        self.__connection.commit()
        print "{} Records Loaded in Insured_Party".format(loadedRecords)
    
    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Insured_Party_Error");
        utils.truncateTable(self.__connection, "Insured_Party_stg");
        utils.truncateLookupTable(self.__connection, "Insured_Party_Lookup", "insured_party_key");
        utils.truncateTable(self.__connection, "Insured_Party_Dimension");
        self.__connection.commit()
        print "\nClean Insured_Party and related staging tables"
        return
            
    def __loadStaging(self):
        utils.truncateTable(self.__connection, "Insured_Party_Error");
        utils.truncateTable(self.__connection, "Insured_Party_stg");
        insertStagingQuery = """
                                insert into Insured_Party_stg
                                (
                                    insured_party_prod_id,
                                    insured_name,
                                    insured_address,
                                    insured_city,
                                    insured_province,
                                    insured_zip,
                                    type,
                                    income,
                                    birth_date,
                                    marital,
                                    gender
                                )
                                select 
                                    insured_party_key,
                                    insured_name,
                                    insured_address,
                                    insured_city,
                                    insured_province,
                                    CONVERT(varchar,insured_zip),
                                    type,
                                    income,
                                    birth_date,
                                    marital,
                                    gender
                                from Insured_Party
                                where insured_party_key is not NULL 
                                order by [insured_city];
                             """
                             
        cursor = self.__connection.cursor()
        cursor.execute(insertStagingQuery)           
        updateRiskColumn = """
                            update Insured_Party_stg
                            set risk_grade = (select risk_grade 
                                              from Policy_Risk
                                              where policy_key = 
                                                  ( select distinct policy_key 
                                                    from Policy_Transaction_Fact policyTxn
                                                    where 
                                                        policyTxn.insured_party_key = Insured_Party_stg.insured_party_prod_id 
                                                  )
                                            )"""
        cursor = self.__connection.cursor()
        cursor.execute(updateRiskColumn)                                                      
        return 
    
    def __updateMappedValues(self):
        mapColsDict = {
                    "insured_city" : "not supported insured_city",
                    "insured_province" : "not supported insured_province"
                    }
        insertErrorTableQuery = """insert into Insured_Party_Error 
                                    select s.*, '{}' from Insured_Party_stg s 
                                    where {} in 
                                        (select value from ColumnValueMappings
                                         where table_name = 'Insured_Party_stg' and column_name = '{}')"""
        cursor = self.__connection.cursor()
        
        for key, value in mapColsDict.items():
            cursor.execute(insertErrorTableQuery.format(value, key, key))
            if cursor.rowcount > 0:
                updateTableQuery = """
                                    update Insured_Party_stg
                                    set {} = 
                                        (select mapped_value from ColumnValueMappings 
                                           where table_name = 'Insured_Party_stg' and column_name = '{}' 
                                           and value = Insured_Party_stg.{})
                                    where 
                                        exists 
                                            (select 1 from ColumnValueMappings
                                             where table_name = 'Insured_Party_stg' and column_name = '{}' 
                                                     and value = Insured_Party_stg.{})"""
                cursor.execute(updateTableQuery.format(key,key,key,key,key))
        return
    
    def __populateProvince(self):
        query = """
                 update Insured_Party_stg
                 set insured_province = 
                     (select province from City_Province_Canada 
                      where City_Province_Canada.city = 
                                  Insured_Party_stg.insured_city) 
                 where insured_province is null or 
                 LTRIM(RTRIM(insured_province)) = ''"""
        cursor = self.__connection.cursor()
        cursor.execute(query)
        return
        
    def __cleanupNulls(self):
        nullColsDict = {
                    'insured_name':['Null insured_name',utils.populateUnknown],
                    'insured_address':['Null insured_address',utils.populateUnknown],
                    'insured_city':['Null insured_city',utils.populateUnknown],
                    'insured_province':['Null insured_province',self.__populateProvince],
                    'insured_zip':['Null insured_zip',utils.populateUnknown],
                    'type':['Null type',utils.populateUnknown],
                    'income':['Null income',utils.populateNegativeNumber],
                    'birth_date':['Null birth_date',utils.populateZeroDate],
                    'marital':['Null marital',utils.populateUnknown],
                    'gender':['Null gender',utils.populateUnknown] ,
                    'risk_grade' : ['Null risk_grade',utils.populateUnknown]
                   }       
        insertErrorTableQuery = """
                                 insert into Insured_Party_Error 
                                 select s.*, '{}'  from Insured_Party_stg s 
                                 where {} is null or LTRIM(RTRIM({})) = ''
                                """
        
        cursor = self.__connection.cursor()
                                 
        for key, value in nullColsDict.items():
            cursor.execute(insertErrorTableQuery.format(value[0], key, key))
            if cursor.rowcount > 0:
                if key == "insured_province":
                    value[1]();
                else :    
                    value[1](self.__connection, "Insured_Party_stg", key)
    
    def __validateCityProvince(self):
        
        errorTableInsertQuery = """insert into Insured_Party_Error 
                                     select s.*, 'Incorrect city-province' 
                                     from Insured_Party_stg s 
                                     where 
                                     not exists ( 
                                         select 1 from City_Province_Canada r 
                                         where s.insured_city = r.city and 
                                               s.insured_province = r.province)"""
        
        cursor = self.__connection.cursor()
        cursor.execute(errorTableInsertQuery)
    
        # update the records with correct province
        if cursor.rowcount > 0:
            updateProvinceQuery = """update Insured_Party_stg 
                                     set insured_province = (select CASE 
                                                                    WHEN COUNT(province) > 1 
                                                                        THEN Insured_Party_stg.insured_province 
                                                                    ELSE 
                                                                        MIN(province) 
                                                                   END 
                                                            from City_Province_Canada r
                                                            where r.city = Insured_Party_stg.insured_city)
                                     where 
                                     insured_city in (select city from City_Province_Canada) 
                                     and 
                                     not exists ( 
                                         select 1 from City_Province_Canada r 
                                         where Insured_Party_stg.insured_city = r.city and 
                                               Insured_Party_stg.insured_province = r.province)"""
                                              
            cursor.execute(updateProvinceQuery)  
            # Validation check for remaining consistencies of city-province combination.
            getMismatchCityProvinceQuery = """select insured_city, insured_province 
                                              from Insured_Party_stg s 
                                              where 
                                                not exists ( 
                                                     select 1 from City_Province_Canada r 
                                                     where s.insured_city = r.city and 
                                                           s.insured_province = r.province) and 
                                                           (insured_city is not null and 
                                                                LTRIM(RTRIM(insured_city)) != '')"""
            
            rows = cursor.execute(getMismatchCityProvinceQuery).fetchall()
            
            if rows:
                print "Following city-province pairs need manual attention. Rolling back the Insured Party Dimension Load"
                for row in rows:
                    print "{}-{}".format(row.insured_city, row.insured_province)
                return False
        return True
    
    def __processDuplicates(self):
        
        # if the name, address, city, province and birthdate are same then records may be duplicates.
        query="""
                select insured_name, insured_address, insured_city, insured_province, birth_date 
                from Insured_Party_stg
                group by insured_name, insured_address, insured_city, insured_province, birth_date 
                having count(*) > 1
              """
        cursor = self.__connection.cursor()
        rows = cursor.execute(query).fetchall()
        if rows:
            duplicateErrorInsertQuery = """
                                         insert into Insured_Party_Error 
                                         select s.*, 'Possible Duplicate' from Insured_Party_stg s
                                         where insured_name = ? and insured_address = ? and 
                                         insured_city = ? and birth_date = ?
                                        """
            
            print "Following records possible duplicates in Insured Party."
            
            for row in rows:
                print "Record:"
                print "insured_name - {} ".format(row.insured_name)
                print "insured_address - {}".format(row.insured_address)
                print "insured_city - {}".format(row.insured_city)
                print "birth_date - {}\n".format(row.birth_date)
                
                cursor.execute(duplicateErrorInsertQuery,
                               row.insured_name, 
                               row.insured_address, 
                               row.insured_city, 
                               row.birth_date);
                
                duplicateRecordIDQuery = """
                                            select insured_party_prod_id 
                                            from Insured_Party_stg 
                                            where 
                                                insured_name = ? and
                                                insured_address = ? and
                                                insured_city = ? and
                                                birth_date = ?
                                        """
                
                cursor.execute(duplicateRecordIDQuery, 
                               row.insured_name, 
                               row.insured_address, 
                               row.insured_city, 
                               row.birth_date)
                
                duplicateIDs = cursor.fetchall()
                
                inClauseList = "("
                for duplicateID in duplicateIDs:
                    
                    if inClauseList != "(":
                        inClauseList += ","
                    inClauseList += str(duplicateID.insured_party_prod_id)
                inClauseList += ")"
                
                deleteInvalidDuplicateIdQry = """delete from Insured_Party_stg
                                                 where
                                                     insured_party_prod_id in 
                                                     """ + inClauseList + """ 
                                                     and insured_party_prod_id not in 
                                                     (select insured_party_key from 
                                                     Claim_Transaction_Fact)"""
                
                cursor.execute(deleteInvalidDuplicateIdQry)
                                                     
            print """Deleted possible records and inserted them in the Insured_Party_Error table with message 'Possible Duplicates'"""    
            
            return True
        
    def __cleanupData(self):
        self.__updateMappedValues()
        returnVal = self.__validateCityProvince()
        if not returnVal:
            return False
        self.__cleanupNulls()
        returnVal = self.__processDuplicates()
        if not returnVal:
            return False
        return True
    
    def __loadLookup(self):
        insertLookupQuery = """insert into Insured_Party_Lookup (insured_party_prod_id)
                                select insured_party_prod_id 
                                from Insured_Party_stg i 
                                        LEFT JOIN Insured_Party_Lookup l
                                        ON (i.insured_party_prod_id = l.insured_party_prod_id)
                                where
                                    l.insured_party_prod_id is NULL""" 
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) 
        
    def __updateType1SCD(self):
        return
    def __updateType2SCD(self):
        return 
        
    def __loadDimensionTable(self):
        
        insertDimensionQuery = """
                                insert into Insured_Party_Dimension 
                                select l.insured_party_key, s.*, NULL 
                                from Insured_Party_Lookup l JOIN Insured_Party_stg s 
                                        ON (l.insured_party_name = s.insured_party_prod_id)"""
                                        
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        
        print "{} rows inserted in Transaction_Dimension".format(cursor.rowcount)
        
        self.__updateType1SCD()
        self.__updateType3SCD()
    
    def __loadLookupAndDimension(self):
        # Select rows from Insured_Party_Key
        count = 0
        selectSourceQuery = """select 
                                   stg.insured_party_prod_id insured_party_prod_id, 
                                   stg.insured_name insured_name, 
                                   stg.insured_address insured_address, 
                                   stg.insured_city insured_city,
                                   stg.insured_province insured_province, 
                                   stg.insured_zip insured_zip, 
                                   stg.type type, 
                                   stg.income income, 
                                   stg.birth_date birth_date, 
                                   stg.marital marital, 
                                   stg.gender gender,
                                   stg.risk_grade risk_grade
                               from Insured_Party_stg stg LEFT JOIN Insured_Party_Dimension dim
                                    ON 
                                    (
                                      stg.insured_party_prod_id = dim.insured_party_prod_id and
                                      stg.insured_name = dim.insured_name and
                                      stg.insured_address = dim.insured_address and
                                      stg.insured_city = dim.insured_city and
                                      stg.insured_province = dim.insured_province and
                                      stg.insured_zip = dim.insured_zip and
                                      stg.type = dim.type and
                                      stg.income = dim.income and
                                      stg.birth_date = dim.birth_date and
                                      stg.marital = dim.marital and
                                      stg.gender = dim.gender and
                                      stg.risk_grade = dim.risk_grade
                                    )
                               where dim.insured_party_key is null
                               order by insured_province, insured_city, insured_party_prod_id
                            """
        
        insuredPartyRowsCursor = self.__connection.cursor()
        insuredPartyRowsCursor.execute(selectSourceQuery)

        for insuredPartyRow in insuredPartyRowsCursor:
            # Find the key in the lookup table if already exists
            selectRecordInLookup = """
                                    select insured_party_key
                                    from Insured_Party_Lookup
                                    where 
                                    insured_party_prod_id = ? and 
                                    insured_name = ? 
                                   """
            commonCursor = self.__connection.cursor()
            commonCursor.execute(selectRecordInLookup, 
                                                   insuredPartyRow.insured_party_prod_id,
                                                   insuredPartyRow.insured_name)
            
            lookupTableRows = commonCursor.fetchall()
            oldInsuredPartyDimensionKey = None
            
            if lookupTableRows:
                oldInsuredPartyDimensionKey = lookupTableRows[0].insured_party_key
                
                # - delete the old record from lookup
                deleteOldRecordQuery = """delete from Insured_Party_Lookup
                                            where insured_party_key = ?"""
                commonCursor.execute(deleteOldRecordQuery, oldInsuredPartyDimensionKey)
            
            # - insert the new record into lookup - new surrogate key gets generated    
            insertLookupQuery = """insert into Insured_Party_Lookup (
                                                                    insured_party_prod_id,
                                                                    insured_name) 
                                    values (?,?)"""
            commonCursor.execute(insertLookupQuery, 
                                 insuredPartyRow.insured_party_prod_id,
                                 insuredPartyRow.insured_name)    
            
            # - get the new surrogate key from lookup
            selectMaxLookupQuery = """select insured_party_key from Insured_Party_Lookup
                                        where 
                                        insured_party_prod_id = ? and
                                        insured_name = ?
                                    """
            commonCursor.execute(selectMaxLookupQuery, 
                                                     insuredPartyRow.insured_party_prod_id,
                                                     insuredPartyRow.insured_name)
            maxLookupKey = commonCursor.fetchone().insured_party_key                        
            
            # - insert the record in the dimension table with the old value in the history column
            insertDimensionQuery = """insert into 
                                        Insured_Party_Dimension(
                                            insured_party_key,
                                            insured_party_prod_id,
                                            insured_name,
                                            insured_address,
                                            insured_city,
                                            insured_province,
                                            insured_zip,
                                            type,
                                            income,
                                            birth_date,
                                            marital,
                                            gender,
                                            risk_grade,
                                            history_record)
                                      values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)"""
            
            commonCursor.execute(insertDimensionQuery,
                                 maxLookupKey,
                                 insuredPartyRow.insured_party_prod_id,
                                 insuredPartyRow.insured_name,
                                 insuredPartyRow.insured_address,
                                 insuredPartyRow.insured_city,
                                 insuredPartyRow.insured_province,
                                 insuredPartyRow.insured_zip,
                                 insuredPartyRow.type,
                                 insuredPartyRow.income,
                                 insuredPartyRow.birth_date,
                                 insuredPartyRow.marital,
                                 insuredPartyRow.gender,
                                 insuredPartyRow.risk_grade,
                                 oldInsuredPartyDimensionKey)
            count += 1
        return count
                 
                        
                