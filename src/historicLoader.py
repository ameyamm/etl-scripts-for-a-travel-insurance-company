import sys
import getopt
import traceback
import db_connect
import etl

dimensionHandlers = {
                    "Transaction" : [etl.loadTransaction, etl.cleanTransaction], 
                    "Transaction_Date" : [etl.loadTransactionDate, etl.cleanTransactionDate],
                    "Insured_Party" : [etl.loadInsuredParty,etl.cleanInsuredParty],
                    "Employee" : [etl.loadEmployee, etl.cleanEmployee],
                    "Plans" : [etl.loadPlan, etl.cleanPlan],
                    "Claim_Procedure" : [etl.loadClaimProcedure, etl.cleanClaimProcedure],
                    "Claimant" : [etl.loadClaimant, etl.cleanClaimant],
                    "Claim" : [etl.loadClaim, etl.cleanClaim],
                    "Third_Party" : [etl.loadThirdParty, etl.cleanThirdParty],
                    "Claim_Transaction_Fact" : [etl.loadClaimTransactionFact, etl.cleanClaimTransactionFact]
                   }

def usage():
    print "ETLDriver.py -h -t <table-name|\"all\">|-c <table-name|\"all\">] "

def validateArgs(optValsDict):
    count = 0 
    if optValsDict["load"] != "":
        count += 1 
    if optValsDict["clean"] != "":
        count += 1
    
    if count > 1:
        print "Specify only one of l/c options"
        usage()
        sys.exit(1)
    elif count < 1:
        print "Specify one of l/c options"
        usage()
        sys.exit(1)
    
def cleanup(cleanupParameter):
    if cleanupParameter == "all":
        connection = db_connect.getFlyAwayDatabaseConnection()
        dimensionHandlers.get("Claim_Transaction_Fact")[1](connection)
        for key, value in dimensionHandlers.items():
            if key != "Claim_Transaction_Fact" :
                value[1](connection)
                print ""
        connection.close()
    elif dimensionHandlers.has_key(cleanupParameter):
        connection = db_connect.getFlyAwayDatabaseConnection()
        try:
            dimensionHandlers.get(cleanupParameter)[1](connection)
        except Exception, err:
            connection.rollback()
            print "Exception Caught"
            traceback.print_exc()
        connection.close()
    else:
        print "Invalid table specified in -c option"
        usage()
        sys.exit(1)
    
    return

def loadTable(loadParameter):
    if loadParameter == "all":
        connection = db_connect.getFlyAwayDatabaseConnection()
        try:
            for key, value in dimensionHandlers.items():
                if key != "Claim_Transaction_Fact" :
                    value[0](connection)
                    print ""
            dimensionHandlers.get("Claim_Transaction_Fact")[0](connection)
        except Exception, err:
            connection.rollback()
            print "Exception Caught"
            traceback.print_exc()
        connection.close()
    elif dimensionHandlers.has_key(loadParameter):
        connection = db_connect.getFlyAwayDatabaseConnection()
        try:
            dimensionHandlers.get(loadParameter)[0](connection)
        except Exception, err:
            connection.rollback()
            print "Exception Caught"
            traceback.print_exc()
        connection.close()
    else :
        print "Invalid table specified in -l option"
        usage()
        sys.exit(1)

def runETL(optValsDict):
    if optValsDict["load"] != "":
        loadTable(optValsDict["load"]);
    elif optValsDict["clean"] != "":
        cleanup(optValsDict["clean"]);
    return
                                
def main(argv):
    try:
        opts, args = getopt.getopt(argv, "hl:c:")
    except getopt.GetoptError:
        usage()
        sys.exit(1)
    
    optValsDict = {
                   "load" : "",
                   "clean" : ""
                  }
    
    for opt, arg in opts:
        if opt == '-h':
            usage()   
            sys.exit(0)
        elif opt == '-l' :
            optValsDict["load"] = arg
        elif opt == '-c' :
            optValsDict["clean"] = arg
    
    # validate args
    validateArgs(optValsDict)
    
    runETL(optValsDict)
    
        
if __name__ == '__main__':
    main(sys.argv[1:])
    