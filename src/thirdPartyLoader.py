import utils

class ThirdPartyLoader(object):
    '''
    loader for Plan Dimension
    '''

    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection

    def loadDimension(self):
        print "Loading Third_Party Dimension"
        self.__loadStagingTable()
        self.__cleanup()
        self.__loadLookup()
        self.__loadDimension()
        self.__connection.commit()
        print "Load Complete"
        
    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Third_Party_stg");
        utils.truncateLookupTable(self.__connection, "Third_Party_Lookup", "third_party_key");
        utils.truncateTable(self.__connection, "Third_Party_Dimension");
        utils.truncateTable(self.__connection, "Third_Party_Error");
        self.__connection.commit()
        print "\nCleaned Third Party and related staging tables"
        return
        
            
    def __loadStagingTable(self):
        utils.truncateTable(self.__connection, "Third_Party_stg");
        utils.truncateTable(self.__connection, "Third_Party_Error");
        insertIntoStagingQuery = """
                                    insert into Third_Party_stg
                                    (
                                        third_party_prod_id,
                                        third_party_name,
                                        third_party_address,
                                        third_party_city,
                                        third_party_country,
                                        third_party_type
                                    ) 
                                    select
                                        Third_party,
                                        third_party_name,
                                        third_party_address,
                                        third_party_city,
                                        third_party_country,
                                        third_party_type
                                    from Third_Party 
                                    where Third_party is not NULL
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoStagingQuery) ;     

    def __cleanup(self):
        self.__updateMappedValues()
        returnVal = self.__validateCityCountry()
        if not returnVal:
            return False
        self.__cleanupNulls()
        if not returnVal:
            return False
        return True
    
    def __updateMappedValues(self):
        mapColsDict = {
                    "third_party_city" : "not supported third_party_city",
                    "third_party_country" : "not supported third_party_country"
                    }
        insertErrorTableQuery = """
                                    insert into Third_Party_Error 
                                    select s.*, '{}' from Third_Party_stg s 
                                    where {} in 
                                        (select value from ColumnValueMappings
                                         where table_name = 'Third_Party_stg' and column_name = '{}')
                                """
        cursor = self.__connection.cursor()
        
        for key, value in mapColsDict.items():
            cursor.execute(insertErrorTableQuery.format(value, key, key))
            if cursor.rowcount > 0:
                updateTableQuery = """
                                    update Third_Party_stg 
                                    set {} = 
                                        (select mapped_value from ColumnValueMappings 
                                           where table_name = 'Third_Party_stg' and column_name = '{}' 
                                           and value = Third_Party_stg.{})
                                    where 
                                        exists 
                                            (select 1 from ColumnValueMappings
                                             where table_name = 'Third_Party_stg' and column_name = '{}' 
                                                     and value = Third_Party_stg.{})
                                    """
                cursor.execute(updateTableQuery.format(key,key,key,key,key))
        return
    
    def __populateCountry(self):
        query = """update Third_Party_stg 
                 set third_party_country = 
                     (select country from City_Province_Country 
                      where City_Province_Country.city = 
                                  Third_Party_stg.third_party_city) 
                 where third_party_country is null or 
                 LTRIM(RTRIM(third_party_country)) = ''"""
        cursor = self.__connection.cursor()
        cursor.execute(query)
        query = """update Third_Party_stg 
                 set third_party_country = 'Unknown' 
                 where third_party_country is null or 
                 LTRIM(RTRIM(third_party_country)) = ''"""
        cursor.execute(query)
        return
        
    def __cleanupNulls(self):
        nullColsDict = {
                    'third_party_name':['Null third_party_name',utils.populateUnknown],
                    'third_party_address':['Null third_party_address',utils.populateUnknown],
                    'third_party_city':['Null third_party_city',utils.populateUnknown],
                    'third_party_country':['Null third_party_country',self.__populateCountry],
                    'third_party_type':['Null third_party_type',utils.populateUnknown]
                   }       
        
        insertCleanupTableQuery = """insert into Third_Party_Error 
                                 select s.*, '{}'  from Third_Party_stg s 
                                 where s.[{}] is null or LTRIM(RTRIM(s.[{}])) = ''"""
        
        cursor = self.__connection.cursor()
                                 
        for key, value in nullColsDict.items():
            cursor.execute(insertCleanupTableQuery.format(value[0], key, key))
            if cursor.rowcount > 0:
                if key == 'third_party_country':
                    value[1]()
                else:
                    value[1](self.__connection, "Third_Party_stg", key)
            
    def __validateCityCountry(self):
        
        errorTableInsertQuery = """insert into Third_Party_Error 
                             select s.*, 'Incorrect city-country' 
                             from Third_Party_stg s 
                             where 
                             not exists ( 
                                 select 1 from City_Province_Country r 
                                        where s.third_party_city = r.city and 
                                              s.third_party_country = r.country)"""
        
        cursor = self.__connection.cursor()
        cursor.execute(errorTableInsertQuery)
    
        # update the records with correct country
        if cursor.rowcount > 0:
            updateCountryQuery = """update Third_Party_stg 
                                    set third_party_country = (select CASE 
                                                                    WHEN COUNT(country) > 1 
                                                                        THEN Third_Party_stg.third_party_country 
                                                                    ELSE 
                                                                        MIN(country) 
                                                                   END 
                                                            from City_Province_Country r
                                                            where r.city = Third_Party_stg.third_party_city)
                                    where 
                                    third_party_city in (select city from City_Province_Country) 
                                    and 
                                    not exists ( 
                                     select 1 from City_Province_Country r 
                                        where Third_Party_stg.third_party_city = r.city and 
                                              Third_Party_stg.third_party_country = r.country)"""
                                              
            cursor.execute(updateCountryQuery)  
            # Validation check for remaining consistencies of city-country combination.
            getMismatchCityCountryQuery = """select third_party_city, third_party_country 
                                            from Third_Party_stg s 
                                            where 
                                            not exists ( 
                                                 select 1 from City_Province_Country r 
                                                 where s.third_party_city = r.city and 
                                                       s.third_party_country = r.country) 
                                            and (third_party_city is not null and LTRIM(RTRIM(third_party_city)) != '')"""
            
            rows = cursor.execute(getMismatchCityCountryQuery).fetchall()
            
            if rows:
                print "Following city-country pairs need manual attention. Rolling back the Third Party Dimension Load"
                for row in rows:
                    print "{}-{}".format(row.third_party_city, row.third_party_country)
                return False
        return True
            
    def __loadLookup(self):
        insertLookupQuery = """
                                insert into Third_Party_Lookup (third_party_prod_id)
                                select s.third_party_prod_id 
                                from Third_Party_stg s LEFT JOIN Third_Party_Lookup l
                                     ON (s.third_party_prod_id = l.third_party_prod_id)
                                where l.third_party_key is null
                            """
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) ;
    
    def __loadDimension(self):
        insertDimensionQuery = """
                                insert into Third_Party_Dimension 
                                select l.third_party_key, s.* 
                                from (
                                        Third_Party_Lookup l JOIN Third_Party_stg s 
                                        ON (l.third_party_prod_id = s.third_party_prod_id)
                                     ) 
                                     LEFT JOIN Third_Party_Dimension dim
                                     ON (l.third_party_key = dim.third_party_key)
                                where dim.third_party_key is null
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        print "{} rows inserted in Third_Party_Dimension".format(cursor.rowcount)
                                
        