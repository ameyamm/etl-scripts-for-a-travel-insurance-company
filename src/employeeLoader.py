import utils

class EmployeeLoader(object):
    '''
    loader for Employee Dimension
    '''

    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection
    
    def loadDimension(self):
        print "Loading Employee Dimension"
        self.__loadStagingTable()
        self.__loadLookup()
        self.__loadDimension()
        self.__connection.commit()
        print "Load Complete"
            
    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Employee_stg");
        utils.truncateLookupTable(self.__connection, "Employee_Lookup", "employee_key");
        utils.truncateTable(self.__connection, "Employee_Dimension");
        self.__connection.commit()
        print "\nClean Employee and related staging tables"
        return
    
    def __loadStagingTable(self):
        utils.truncateTable(self.__connection, "Employee_stg");
        insertIntoStagingQuery = """
                                    insert into Employee_stg
                                    (
                                        employee_prod_id,
                                        employee_name,
                                        employee_type,
                                        employee_department
                                    ) 
                                    select
                                        employee_NR,
                                        employee_name,
                                        employee_type,
                                        department
                                    from Employee 
                                    where employee_NR is not NULL;
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoStagingQuery) ;
    
    def __loadLookup(self):
        insertLookupQuery = """
                                insert into Employee_Lookup (employee_prod_id)
                                select s.employee_prod_id 
                                from Employee_stg s LEFT JOIN Employee_Lookup l
                                     ON (s.employee_prod_id = l.employee_prod_id)
                                where l.employee_key is NULL
                                order by s.employee_department, s.employee_type, s.employee_name"""
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) ;
    
    def __loadDimension(self):
        insertDimensionQuery = """
                                insert into Employee_Dimension 
                                select l.employee_key, s.* 
                                from (
                                        Employee_Lookup l JOIN Employee_stg s 
                                        ON (l.employee_prod_id = s.employee_prod_id)
                                     ) 
                                     LEFT JOIN Employee_Dimension dim
                                     ON (l.employee_key = dim.employee_key)
                                where 
                                    dim.employee_key is NULL
                                order by s.employee_department, s.employee_type, s.employee_name
                               """
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        print "{} rows inserted in Employee_Dimension".format(cursor.rowcount)
                                
        