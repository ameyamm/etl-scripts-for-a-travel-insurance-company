import utils

class ClaimLoader(object):
    '''
    loader for Claim Dimension
    '''

    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection
    
    def loadDimension(self):
        print "Loading Claim Dimension"
        self.__loadStagingTable()
        self.__cleanup()
        self.__loadLookup()
        self.__loadDimension()
        self.__connection.commit()
        print "Load Complete"
  
    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Claim_stg");
        utils.truncateLookupTable(self.__connection, "Claim_Lookup", "claim_key");
        utils.truncateTable(self.__connection, "Claim_Dimension");
        utils.truncateTable(self.__connection, "Claim_Cleanup_Records");
        self.__connection.commit()
        print "\nCleaned Claim and related staging tables"
        return
        
            
    def __loadStagingTable(self):
        utils.truncateTable(self.__connection, "Claim_stg");
        utils.truncateTable(self.__connection, "Claim_Cleanup_Records");
        insertIntoStagingQuery = """
                                    insert into Claim_stg
                                    (
                                        claim_prod_id,
                                        claim_description,
                                        claim_type
                                    ) 
                                    select
                                        Claim_ID,
                                        claim_description,
                                        claim_type
                                    from Claim 
                                    where Claim_ID is not NULL
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoStagingQuery) ;
        
    def __cleanup(self):
        
        nullColsDict = {
                        'claim_description':['Null claim_description ',utils.populateUnknown],
                       }       
        
        insertCleanupTableQuery = """insert into Claim_Cleanup_Records 
                                 select s.*, '{}'  from Claim_stg s 
                                 where s.[{}] is null or LTRIM(RTRIM(s.[{}])) = ''"""
        
        cursor = self.__connection.cursor()
                                 
        for key, value in nullColsDict.items():
            cursor.execute(insertCleanupTableQuery.format(value[0], key, key))
            if cursor.rowcount > 0:
                value[1](self.__connection, "Claim_stg", key)
                  
    def __loadLookup(self):
        insertLookupQuery = """insert into Claim_Lookup (claim_prod_id)
                                select stg.claim_prod_id 
                                from Claim_stg stg LEFT JOIN Claim_Lookup lk
                                     ON (stg.claim_prod_id = lk.claim_prod_id)
                                where lk.claim_key is null
                                order by 1"""
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) ;
    
    def __loadDimension(self):
        insertDimensionQuery = """insert into Claim_Dimension 
                                    select l.claim_key, s.* 
                                    from (
                                            Claim_Lookup l 
                                             JOIN Claim_stg s 
                                             ON (l.claim_prod_id = s.claim_prod_id)
                                         ) 
                                         LEFT JOIN Claim_Dimension dim 
                                         ON (dim.claim_key = l.claim_key)
                                    where dim.claim_key is NULL
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        print "{} rows inserted in Claim_Dimension".format(cursor.rowcount)
                                
        