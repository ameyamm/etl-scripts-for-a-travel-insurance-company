import utils

class ClaimTransactionFactLoader:
    '''
    Loads the Claim Transaction Fact
    '''

    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection
    
    def loadFactTable(self):
        print "Loading Claim Transaction Fact"
        self.__loadStagingTable()
        self.__deduplicate()
        #self.__checkRI()
        self.__loadFactTable()
        self.__connection.commit()
        print "Load Complete"
        
    def cleanFactTable(self):
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_stg_orig");
        utils.truncateTable(self.__connection, "Claim_Coverage_Info_stg");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_stg_combined");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_stg_final");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_Error");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_Combined_Error");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_Table");
        self.__connection.commit()
        print "\nClean Claim Transaction Fact and related staging tables"
        return
    
    def __loadStagingTable(self):
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_stg_orig");
        utils.truncateTable(self.__connection, "Claim_Coverage_Info_stg");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_stg_combined");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_stg_final");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_Error");
        utils.truncateTable(self.__connection, "Claim_Transaction_Fact_Combined_Error");
        insertIntoClaimTransactionFactStg = """
                                            insert into Claim_Transaction_Fact_stg_orig
                                            (
                                                date_prod_id,
                                                insured_party_prod_id,
                                                employee_prod_id,
                                                claimant_prod_id,
                                                claim_prod_id,
                                                third_party_prod_id,
                                                claim_trans_prod_id,
                                                amount
                                            ) 
                                            select
                                                date_key,
                                                insured_party_key,
                                                employee_Nr,
                                                Claimant_ID,
                                                Claim_ID,
                                                Third_party,
                                                claim_trans_key,
                                                amount
                                            from Claim_Transaction_Fact
                                            """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoClaimTransactionFactStg) ;
        
        insertIntoClaimCoverageInfoStg = """
                                            insert into Claim_Coverage_Info_stg
                                            (
                                                date_prod_id,
                                                insured_party_prod_id,
                                                covered_item_prod_id,
                                                coverage_prod_id,
                                                amount
                                            ) 
                                            select
                                                date_key,
                                                insured_party_key,
                                                covrd_item_key,
                                                coverage_key,
                                                amount
                                            from Claim_Coverage_Info
                                            order by insured_party_key, date_key, amount 
                                        """
        cursor.execute(insertIntoClaimCoverageInfoStg) ;
        
        self.__checkRI()
        
        insertIntoClaimTransactionFactRIStg = """
                                                insert into Claim_Transaction_Fact_stg_RI
                                                (
                                                    date_prod_id,
                                                    insured_party_prod_id,
                                                    employee_prod_id,
                                                    claimant_prod_id,
                                                    claim_prod_id,
                                                    third_party_prod_id,
                                                    claim_trans_prod_id,
                                                    amount
                                                ) 
                                                select
                                                    date_prod_id,
                                                    insured_party_prod_id,
                                                    employee_prod_id,
                                                    claimant_prod_id,
                                                    claim_prod_id,
                                                    third_party_prod_id,
                                                    claim_trans_prod_id,
                                                    amount
                                                from Claim_Transaction_Fact_stg_orig
                                                order by insured_party_prod_id, date_prod_id, amount 
                                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoClaimTransactionFactRIStg) ;
        
        insertIntoClaimTransactionFactCombinedStg = """
                                                    insert into Claim_Transaction_Fact_stg_combined
                                                    (
                                                        date_prod_id,
                                                        insured_party_prod_id,
                                                        employee_prod_id,
                                                        claimant_prod_id,
                                                        claim_prod_id,
                                                        third_party_prod_id,
                                                        claim_trans_prod_id,
                                                        covered_item_prod_id,
                                                        coverage_prod_id,
                                                        amount
                                                    )
                                                    select 
                                                        txn.date_prod_id,
                                                        txn.insured_party_prod_id,
                                                        txn.employee_prod_id,
                                                        txn.claimant_prod_id,
                                                        txn.claim_prod_id,
                                                        txn.third_party_prod_id,
                                                        txn.claim_trans_prod_id,
                                                        cov.covered_item_prod_id,
                                                        cov.coverage_prod_id,
                                                        txn.amount
                                                    from 
                                                        Claim_Transaction_Fact_stg_RI txn 
                                                        JOIN 
                                                        Claim_Coverage_Info_stg cov
                                                        ON (txn.row_seq = cov.row_seq)
                                                    """
        cursor.execute(insertIntoClaimTransactionFactCombinedStg) ;
        return                                                
    
    def __deduplicate(self):
        insertDuplicateRowsInError = """
                                        insert into Claim_Transaction_Fact_Combined_Error
                                        (    
                                            date_prod_id,
                                            insured_party_prod_id,
                                            employee_prod_id,
                                            claimant_prod_id,
                                            claim_prod_id,
                                            third_party_prod_id,
                                            claim_trans_prod_id,
                                            covered_item_prod_id,
                                            coverage_prod_id,
                                            amount, 
                                            error_reason
                                        )
                                        select
                                             date_prod_id,
                                             insured_party_prod_id,
                                             employee_prod_id,
                                             claimant_prod_id,
                                             claim_prod_id,
                                             third_party_prod_id,
                                             claim_trans_prod_id,
                                             covered_item_prod_id,
                                             coverage_prod_id,
                                             amount,
                                             'Duplicate rows'
                                        from
                                            Claim_Transaction_Fact_stg_combined
                                        group by 
                                             date_prod_id,
                                             insured_party_prod_id,
                                             employee_prod_id,
                                             claimant_prod_id,
                                             claim_prod_id,
                                             third_party_prod_id,
                                             claim_trans_prod_id,
                                             covered_item_prod_id,
                                             coverage_prod_id,
                                             amount
                                        having COUNT(*) > 1;
                                     """   
        cursor = self.__connection.cursor()
        cursor.execute(insertDuplicateRowsInError) ;
        
        loadFinalStagedTableQry = """
                                    insert into Claim_Transaction_Fact_stg_final
                                    (    
                                        date_prod_id,
                                        insured_party_prod_id,
                                        employee_prod_id,
                                        claimant_prod_id,
                                        claim_prod_id,
                                        third_party_prod_id,
                                        claim_trans_prod_id,
                                        coverage_prod_id,
                                        covered_item_prod_id,
                                        amount
                                    )
                                    select
                                         ISNULL(date_prod_id,0),
                                         ISNULL(insured_party_prod_id,-1),
                                         ISNULL(employee_prod_id,''),
                                         ISNULL(claimant_prod_id,''),
                                         ISNULL(claim_prod_id,-1),
                                         ISNULL(third_party_prod_id,-1),
                                         ISNULL(claim_trans_prod_id,-1),
                                         ISNULL(coverage_prod_id,-1),
                                         ISNULL(covered_item_prod_id,-1),
                                         amount
                                    from
                                        Claim_Transaction_Fact_stg_combined
                                    group by 
                                         date_prod_id,
                                         insured_party_prod_id,
                                         employee_prod_id,
                                         claimant_prod_id,
                                         claim_prod_id,
                                         third_party_prod_id,
                                         claim_trans_prod_id,
                                         coverage_prod_id,
                                         covered_item_prod_id,
                                         amount
                                  """  
        cursor.execute(loadFinalStagedTableQry) ;
        return
    
    def __checkRI(self):
        colsToCheckClaimTransactionFact = {
                                           "date_prod_id" : ["Transaction_Date_Lookup", self.__dateRICorrector],
                                           "insured_party_prod_id" : ["Insured_Party_Dimension", self.__insuredPartyRICorrector],
                                           "employee_prod_id" : ["Employee_Dimension", self.__employeeRICorrector],
                                           "claimant_prod_id" : ["Claimant_Dimension", self.__claimantRICorrector] ,
                                           "claim_prod_id" : ["Claim_Dimension", self.__claimRICorrector],
                                           "third_party_prod_id" : ["Third_Party_Dimension", self.__thirdPartyRICorrector],
                                           "claim_trans_prod_id" : ["Transaction_Dimension", self.__transactionRICorrector],
                                           }
        
        srcTable = "Claim_Transaction_Fact_stg_orig"
        errorTable = "Claim_Transaction_Fact_Error"
        
        for key,value in colsToCheckClaimTransactionFact.items():
            violatesRI = self.__checkRIForField(key,srcTable,value[0])
            if violatesRI == False:
                value[1](key,srcTable,value[0],errorTable)
                
        colsToCheckForClaimCoverageInfo = {
                       "date_prod_id" : ["Transaction_Date_Lookup", self.__dateRICorrector],
                       "coverage_prod_id" : ["Plan_Dimension", self.__planRICorrector],
                       "covered_item_prod_id" : ["Claim_Procedure_Dimension", self.__coveredItemRICorrector]
                       }
        srcTable = "Claim_Coverage_Info_stg"
        errorTable = "Claim_Coverage_Info_Error"
        
        for key,value in colsToCheckForClaimCoverageInfo.items():
            violatesRI = self.__checkRIForField(key,srcTable,value[0])
            if violatesRI == False:
                value[1](key,srcTable,value[0],errorTable)
        return
    
    def __checkRIForField(self, column, srcTable, refTable):
        checkRIQuery = """
                        select s.{} {} from {} s
                        where s.{} not in (select r.{} from {} r ) ;    
                       """ 
        cursor = self.__connection.cursor()
        cursor.execute(checkRIQuery.format(column,column,srcTable,column, column, refTable))
        rows  = cursor.fetchall()
        if rows :
            return False
        
        return True
    
    def __defaultRICorrection(self, column, srcTable, refTable, errorTable, defaultValue):
        insertErrorTableQuery = """
                                    insert into {}
                                    select s.* , 'Referential Integrity violation for {}'
                                    from {} s
                                    where {} not in (select {} from {});
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertErrorTableQuery.format(errorTable, column,srcTable,column, column, refTable))
        insertedRows = cursor.rowcount 
        
        updateDateForViolatedRows = """update {}
                                       set {} = {}
                                       where {} not in (select {} from {});
                                    """
        
        cursor.execute(updateDateForViolatedRows.format(srcTable, column, defaultValue, column, column, refTable))
        updatedRowsCount = cursor.rowcount  
        if updatedRowsCount != insertedRows :
            print "RI {} not corrected properly.".format(column)
        return
    
    def __dateRICorrector(self, column, srcTable, refTable, errorTable):
        self.__defaultRICorrection(column, srcTable, refTable, errorTable, 0)                          
        return
    
    def __insuredPartyRICorrector(self, column, srcTable, refTable, errorTable):
        insertErrorTableQuery = """
                                    insert into {}
                                    select s.* , 'Referential Integrity violation for {}'
                                    from Claim_Transaction_Fact_stg_orig s
                                    where {} not in 
                                        ( select {} from {} );
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertErrorTableQuery.format(errorTable, column, column, column, refTable))
        insertedRows = cursor.rowcount 
        
        updateViolatedRows = """
                              update {}
                              set insured_party_prod_id = (
                                  select ISNULL(insured_party_prod_id, {}.insured_party_prod_id)
                                  from {} c1
                                  where exists 
                                       (
                                          select 1 from {} c2
                                          where c1.claim_prod_id = c2.claim_prod_id and
                                          c1.claimant_prod_id = c2.claimant_prod_id and
                                          c1.third_party_prod_id = c2.third_party_prod_id and
                                          c1.employee_prod_id = c2.employee_prod_id and
                                          c2.insured_party_prod_id not in 
                                              (select insured_party_prod_id 
                                               from Insured_Party_Dimension)
                                        ) and
                                        c1.insured_party_prod_id in (
                                            select insured_party_prod_id from Insured_Party_Dimension)
                                        group by insured_party_prod_id
                                  )      
                              where
                                   insured_party_prod_id not in (
                                          select insured_party_prod_id from Insured_Party_Dimension);
                            """.format(srcTable, srcTable, srcTable, srcTable)

        cursor.execute(updateViolatedRows)
        updatedRowsCount = cursor.rowcount
        if updatedRowsCount != insertedRows :
            print "Insured Party RI not corrected properly."                            
        return 
    
    def __employeeRICorrector(self, column, srcTable, refTable, errorTable):
        self.__defaultRICorrection(column, srcTable, refTable, errorTable, "\'\'")                        
        return 
    
    def __claimantRICorrector(self,column, srcTable, refTable, errorTable):
        insertErrorTableQuery = """
                                    insert into {}
                                    select s.* , 'Referential Integrity violation for {}'
                                    from {} s
                                    where {} not in 
                                        ( select {} from {} );
                                """.format(errorTable, column, srcTable, column, column, refTable)
                                
        cursor = self.__connection.cursor()
        cursor.execute(insertErrorTableQuery)
        insertedRows = cursor.rowcount 
        
        updateViolatedRows = """
                              update {}
                              set claimant_prod_id = (
                                  select ISNULL(claimant_prod_id, {}.claimant_prod_id)
                                  from {} c1
                                  where exists 
                                       (
                                          select 1 from {} c2
                                          where c1.claim_prod_id = c2.claim_prod_id and
                                          c1.insured_party_prod_id = c2.insured_party_prod_id and
                                          c1.third_party_prod_id = c2.third_party_prod_id and
                                          c1.employee_prod_id = c2.employee_prod_id and
                                          c2.claimant_prod_id not in 
                                              (select claimant_prod_id 
                                               from Claimant_Dimension)
                                        ) and
                                        c1.claimant_prod_id in (
                                            select claimant_prod_id from Claimant_Dimension)
                                        group by claimant_prod_id
                                  )      
                              where
                                   claimant_prod_id not in (
                                          select claimant_prod_id from Claimant_Dimension);
                            """.format(srcTable, srcTable, srcTable, srcTable)

        cursor.execute(updateViolatedRows)
        updatedRowsCount = cursor.rowcount
        if updatedRowsCount != insertedRows :
            print "Claimant RI not corrected properly."                            
        return 
    
    def __claimRICorrector(self,column,srcTable, refTable, errorTable):
        self.__defaultRICorrection(column,srcTable,  refTable, errorTable, -1)                         
        return 
    
    def __thirdPartyRICorrector(self,column,srcTable, refTable, errorTable):
        self.__defaultRICorrection(column, srcTable, refTable, errorTable, -1)                         
        return 
    
    def __transactionRICorrector(self,column,srcTable, refTable, errorTable):
        self.__defaultRICorrection(column,srcTable,  refTable, errorTable, -1)                         
        return 
    
    def __planRICorrector(self,column,srcTable, refTable, errorTable):
        self.__defaultRICorrection(column, srcTable, refTable, errorTable, -1)  
        return
    
    def __coveredItemRICorrector(self, column, srcTable, refTable, errorTable):
        self.__defaultRICorrection(column, srcTable,  refTable, errorTable, -1)  
        return
        
    def __loadFactTable(self):
        insertFactQuery = """
                                insert into Claim_Transaction_Fact_Table
                                select 
                                    date_LP.transaction_date_key, 
                                    plan_LP.plan_key ,
                                    insured_party_LP.insured_party_key,
                                    claim_LP.claim_key,
                                    claim_proc_LP.claim_procedure_key,
                                    txn_LP.transaction_key,
                                    claimant_LP.claimant_key,
                                    third_party_LP.third_party_key,
                                    emp_LP.employee_key,
                                    fact.amount
                                from 
                                    (
                                        (
                                            (
                                                (
                                                    (
                                                        (
                                                            (
                                                                (
                                                                    (
                                                                        (  
                                                                            (
                                                                                Claim_Transaction_Fact_stg_final fact 
                                                                                JOIN Transaction_Date_Lookup date_LP 
                                                                                ON (fact.date_prod_id = date_LP.date_prod_id)
                                                                            )  
                                                                            JOIN Plan_Lookup plan_LP 
                                                                            ON (fact.coverage_prod_id = plan_LP.coverage_prod_id)
                                                                        ) 
                                                                       JOIN Insured_Party_Dimension insured_party_dim
                                                                        ON (fact.insured_party_prod_id = insured_party_dim.insured_party_prod_id
                                                                            and exists (select 1 from Insured_Party_Lookup 
                                                                                        where Insured_Party_Lookup.insured_party_key = insured_party_dim.insured_party_key))
                                                                    )
                                                                    JOIN Insured_Party_Lookup insured_party_LP
                                                                    ON (
                                                                        insured_party_dim.insured_party_prod_id = insured_party_LP.insured_party_prod_id and
                                                                        insured_party_dim.insured_party_key = insured_party_LP.insured_party_key and
                                                                        insured_party_dim.insured_name = insured_party_LP.insured_name 
                                                                        )
                                                                )
                                                                JOIN Claim_Lookup claim_LP
                                                                ON (fact.claim_prod_id = claim_LP.claim_prod_id)
                                                            )
                                                            JOIN Claim_Procedure_Lookup claim_proc_LP
                                                            ON (fact.covered_item_prod_id = claim_proc_LP.covered_item_prod_id)
                                                        ) 
                                                        JOIN Transaction_Lookup txn_LP
                                                        ON (fact.claim_trans_prod_id = txn_LP.claim_trans_prod_id)
                                                    )
                                                    JOIN Claimant_Dimension claimant_dim
                                                    ON (fact.claimant_prod_id = claimant_dim.claimant_prod_id 
                                                        and exists (select 1 from Claimant_Lookup 
                                                                    where Claimant_Lookup.claimant_key = claimant_dim.claimant_key))
                                                )                    
                                                JOIN Claimant_Lookup claimant_LP
                                                ON (claimant_dim.claimant_prod_id = claimant_LP.claimant_prod_id and
                                                    claimant_dim.claimant_name = claimant_LP.claimant_name)
                                            )
                                            JOIN Third_Party_Lookup third_party_LP
                                            ON (fact.third_party_prod_id = third_party_LP.third_party_prod_id)
                                        ) 
                                        JOIN Employee_Lookup emp_LP
                                        ON (fact.employee_prod_id = emp_LP.employee_prod_id)
                                    )
                                    LEFT JOIN Claim_Transaction_Fact_Table oldFact
                                    ON (
                                        date_LP.transaction_date_key = oldFact.transaction_date_key and 
                                        plan_LP.plan_key  = oldFact.plan_key  and
                                        insured_party_LP.insured_party_key = oldFact.insured_party_key and
                                        claim_LP.claim_key = oldFact.claim_key and
                                        claim_proc_LP.claim_procedure_key = oldFact.claim_procedure_key and
                                        txn_LP.transaction_key = oldFact.transaction_key and
                                        claimant_LP.claimant_key = oldFact.claimant_key and
                                        third_party_LP.third_party_key = oldFact.third_party_key and
                                        emp_LP.employee_key = oldFact.employee_key
                                        )
                                where 
                                    oldFact.amount is null
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertFactQuery)
        print "{} rows inserted in Claim_Transaction_Fact_Table".format(cursor.rowcount)
        return