import transactionLoader
import transactionDateLoader
import insuredPartyLoader
import employeeLoader
import planLoader
import claimProcedureLoader
import claimantLoader
import claimLoader
import thirdPartyLoader
import claimTransactionFactLoader

def loadTransaction(connection):
    loader = transactionLoader.TransactionLoader(connection)
    loader.loadDimension()
    return

def cleanTransaction(connection):
    loader = transactionLoader.TransactionLoader(connection)
    loader.cleanDimension()
    return

def loadTransactionDate(connection):
    loader = transactionDateLoader.TransactionDateLoader(connection)
    loader.loadDimension()
    return

def cleanTransactionDate(connection):
    loader = transactionDateLoader.TransactionDateLoader(connection)
    loader.cleanDimension()
    return

def loadInsuredParty(connection):
    loader = insuredPartyLoader.InsuredPartyLoader(connection)
    returnVal = loader.loadDimension()
    return returnVal

def cleanInsuredParty(connection):
    loader = insuredPartyLoader.InsuredPartyLoader(connection)
    loader.cleanDimension()
    return 

def loadEmployee(connection):
    loader = employeeLoader.EmployeeLoader(connection)
    loader.loadDimension()
    return

def cleanEmployee(connection):
    loader = employeeLoader.EmployeeLoader(connection)
    loader.cleanDimension()
    return

def loadPlan(connection):
    loader = planLoader.PlanLoader(connection)
    loader.loadDimension()
    return

def cleanPlan(connection):
    loader = planLoader.PlanLoader(connection)
    loader.cleanDimension()
    return

def loadClaimProcedure(connection):
    loader = claimProcedureLoader.ClaimProcedureLoader(connection)
    loader.loadDimension()
    return

def cleanClaimProcedure(connection):
    loader = claimProcedureLoader.ClaimProcedureLoader(connection)
    loader.cleanDimension()
    return

def loadClaimant(connection):
    loader = claimantLoader.ClaimantLoader(connection)
    loader.loadDimension()
    return

def cleanClaimant(connection):
    loader = claimantLoader.ClaimantLoader(connection)
    loader.cleanDimension()
    return

def loadClaim(connection):
    loader = claimLoader.ClaimLoader(connection)
    loader.loadDimension()
    return

def cleanClaim(connection):
    loader = claimLoader.ClaimLoader(connection)
    loader.cleanDimension()
    return

def loadThirdParty(connection):
    loader = thirdPartyLoader.ThirdPartyLoader(connection)
    loader.loadDimension()
    return

def cleanThirdParty(connection):
    loader = thirdPartyLoader.ThirdPartyLoader(connection)
    loader.cleanDimension()
    return

def loadClaimTransactionFact(connection):
    loader = claimTransactionFactLoader.ClaimTransactionFactLoader(connection)
    loader.loadFactTable()
    return
    ''' Claimant ID - Ji0001 '''
    ''' Insured_Party_Key = 171 '''
    '''
    update Claim_Transaction_Fact 
set insured_party_key = (
select insured_party_key from Claim_Transaction_Fact c1
where exists (
select 1 from Claim_Transaction_Fact c2
where c1.Claim_ID = c2.Claim_ID and
c1.Claimant_ID = c2.Claimant_ID and
c1.Third_party = c2.Third_party and
c1.employee_Nr = c2.employee_Nr and
c2.insured_party_key not in (select insured_party_key from Insured_Party)) and
c1.insured_party_key in (select insured_party_key from Insured_Party)
group by insured_party_key)
where 
insured_party_key not in (select insured_party_key from Insured_Party)
'''
    
    '''
    update Claim_Transaction_Fact
set Claimant_ID = (
                    select Claimant_ID 
                    from Claim_Transaction_Fact c1
                    where exists (
                        select 1 
                        from Claim_Transaction_Fact c2
                        where c1.insured_party_key = c2.insured_party_key and
                                c1.Claim_ID = c2.Claim_ID and
                                c1.Third_party = c2.Third_party and
                                c1.employee_Nr = c2.employee_Nr and
                                c2.Claimant_ID not in 
                                    (select Claimant_ID from Claimant)) and
                                c1.Claimant_ID in (select Claimant_ID from Claimant)
                        group by Claimant_ID)
where 
Claimant_ID not in (select Claimant_ID from Claimant)
'''

def cleanClaimTransactionFact(connection):
    loader = claimTransactionFactLoader.ClaimTransactionFactLoader(connection)
    loader.cleanFactTable()
    return