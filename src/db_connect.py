import pyodbc

def getFlyAwayDatabaseConnection():
    driverstring="SQL Server Native Client 10.0"
    db="FlyAway_Data_Staging"
    servername="localhost"
    connection = pyodbc.connect(driver=driverstring,server=servername,database=db,Trusted_connection='yes', MARS_Connection='yes')
    return connection


