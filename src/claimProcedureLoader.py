import utils

class ClaimProcedureLoader(object):
    '''
    loader for Claim_Procedure Dimension
    '''

    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection
    
    def loadDimension(self):
        print "Loading Claim_Procedure Dimension"
        self.__loadStagingTable()
        self.__cleanup()
        self.__loadLookup()
        self.__loadDimension()
        self.__connection.commit()
        print "Load Complete"
        
    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Claim_Procedure_stg");
        utils.truncateTable(self.__connection, "Claim_Procedure_Cleanup_Records");
        utils.truncateLookupTable(self.__connection, "Claim_Procedure_Lookup", "claim_procedure_key");
        utils.truncateTable(self.__connection, "Claim_Procedure_Dimension");
        self.__connection.commit()
        print "\nCleaned Claim Procedure and related staging tables"
        return
        
            
    def __loadStagingTable(self):
        utils.truncateTable(self.__connection, "Claim_Procedure_stg");
        utils.truncateTable(self.__connection, "Claim_Procedure_Cleanup_Records");
        insertIntoStagingQuery = """
                                    insert into Claim_Procedure_stg
                                    (
                                        covered_item_prod_id,
                                        covered_item,
                                        severity,
                                        coverage_type
                                    ) 
                                    select
                                        covrd_item_key,
                                        [covered item],
                                        severity,
                                        coverage_type
                                    from Claim_Procedure 
                                    where covrd_item_key is not NULL
                                    order by covrd_item_key ;
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoStagingQuery) ;
    
    def __cleanup(self):
        nullColsDict = {
                        'severity':['Null Severity',utils.populateNegativeNumber],
                       }       
        
        insertCleanupTableQuery = """insert into Claim_Procedure_Cleanup_Records 
                                 select s.*, '{}'  from Claim_Procedure_stg s 
                                 where s.[{}] is null or LTRIM(RTRIM(s.[{}])) = ''"""
        
        cursor = self.__connection.cursor()
                                 
        for key, value in nullColsDict.items():
            cursor.execute(insertCleanupTableQuery.format(value[0], key, key))
            if cursor.rowcount > 0:
                value[1](self.__connection, "Claim_Procedure_stg", key)
                
            
    def __loadLookup(self):
        insertLookupQuery = """insert into Claim_Procedure_Lookup (covered_item_prod_id)
                                select stg.covered_item_prod_id 
                                from Claim_Procedure_stg stg LEFT JOIN CLaim_Procedure_Lookup lp
                                     ON (stg.covered_item_prod_id = lp.covered_item_prod_id)
                                where lp.claim_procedure_key is NULL 
                                order by 1"""
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) ;
    
    def __loadDimension(self):
        insertDimensionQuery = """
                                insert into Claim_Procedure_Dimension 
                                select l.claim_procedure_key, s.* 
                                from (
                                        Claim_Procedure_Lookup l JOIN Claim_Procedure_stg s 
                                        ON (l.covered_item_prod_id = s.covered_item_prod_id)
                                     ) 
                                     LEFT JOIN Claim_Procedure_Dimension dim
                                     ON (l.claim_procedure_key = dim.claim_procedure_key)
                                where 
                                    dim.claim_procedure_key is NULL
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        print "{} rows inserted in Claim_Procedure_Dimension".format(cursor.rowcount)
                                
        