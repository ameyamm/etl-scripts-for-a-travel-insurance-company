import utils

class PlanLoader(object):
    '''
    loader for Plan Dimension
    '''

    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection
    
                
    def loadDimension(self):
        print "Loading Plan Dimension"
        self.__loadStagingTable()
        self.__cleanup()
        self.__loadLookup()
        self.__loadDimension()
        self.__connection.commit()
        print "Load Complete"

    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Plan_Cleanup_Records");
        utils.truncateTable(self.__connection, "Plan_stg");
        utils.truncateLookupTable(self.__connection, "Plan_Lookup", "plan_key");
        utils.truncateTable(self.__connection, "Plan_Dimension");
        self.__connection.commit()
        print "\nCleaned Plan and related staging tables"
        return
        
            
    def __loadStagingTable(self):
        utils.truncateTable(self.__connection, "Plan_Cleanup_Records");
        utils.truncateTable(self.__connection, "Plan_stg");
        insertIntoStagingQuery = """
                                    insert into Plan_stg
                                    (
                                        coverage_prod_id,
                                        coverage_description,
                                        marital_status,
                                        coverage,
                                        premium,
                                        age_range
                                    ) 
                                    select
                                        [coverage_key],
                                        [coverage_description],
                                        [Marital Status],
                                        [Coverage],
                                        [Premium],
                                        [Age Range]
                                    from Plans 
                                    where coverage_key is not NULL
                                    order by coverage_key ;
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoStagingQuery) ;


    def __cleanup(self):
        nullColsDict = {
                    'coverage':['Null Coverage',utils.populateNegativeNumber],
                    'premium':['Null Premium',utils.populateNegativeNumber],
                    'age_range':['Null Age Range',utils.populateNA],
                   }       
        
        insertCleanupTableQuery = """
                                    insert into Plan_Cleanup_Records  
                                     select s.*, '{}'  from Plan_stg s 
                                     where s.[{}] is null or LTRIM(RTRIM(s.[{}])) = ''"""
        
        cursor = self.__connection.cursor()
                                 
        for key, value in nullColsDict.items():
            cursor.execute(insertCleanupTableQuery.format(value[0], key, key))
            if cursor.rowcount > 0:
                value[1](self.__connection, "Plan_stg", key)

            
    def __loadLookup(self):
        insertLookupQuery = """insert into Plan_Lookup (coverage_prod_id)
                                select s.coverage_prod_id 
                                from Plan_stg s LEFT JOIN Plan_Lookup l 
                                     ON ( s.coverage_prod_id = l.coverage_prod_id )
                                where l.plan_key is NULL
                                order by s.coverage_prod_id"""
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) ;
    
    def __loadDimension(self):
        insertDimensionQuery = """
                                insert into Plan_Dimension 
                                select l.plan_key, s.* 
                                from (
                                        Plan_Lookup l JOIN Plan_stg s 
                                        ON (l.coverage_prod_id = s.coverage_prod_id)
                                     )
                                     LEFT JOIN Plan_Dimension dim 
                                     ON (l.plan_key = dim.plan_key)
                                where dim.plan_key is NULL
                                """
                                
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        print "{} rows inserted in Plan_Dimension".format(cursor.rowcount)
                                
        