import utils

class ClaimantLoader(object):
    '''
    loader for Claimant Dimension
    '''

    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection
    
    def loadDimension(self):
        print "Loading Claimant Dimension"
        self.__loadStagingTable()
        returnVal = self.__cleanup()
        if not returnVal:
            self.__connection.rollback()
            return
        self.__loadLookup()
        self.__loadDimension()
        self.__connection.commit()
        print "Load Complete"

    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Claimant_stg");
        utils.truncateLookupTable(self.__connection, "Claimant_Lookup", "claimant_key");
        utils.truncateTable(self.__connection, "Claimant_Dimension");
        self.__connection.commit()
        print "\nCleaned Claimant and related staging tables"
        return
        
            
    def __loadStagingTable(self):
        utils.truncateTable(self.__connection, "Claimant_stg");
        insertIntoStagingQuery = """
                                    insert into Claimant_stg
                                    (
                                        claimant_prod_id,
                                        claimant_name,
                                        claimant_address,
                                        claimant_city,
                                        claimant_province,
                                        claimant_zip
                                    ) 
                                    select
                                        Claimant_ID,
                                        claimant_name,
                                        claimant_address,
                                        Claimant_city,
                                        Claimant_province,
                                        LTRIM(STR(claimant_zip,7,0))
                                    from Claimant 
                                    where Claimant_ID is not NULL
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoStagingQuery) ;
    
    def __updateMappedValues(self):
        mapColsDict = {
                    "claimant_city" : "not supported claimant_city",
                    "claimant_province" : "not supported claimant_province"
                    }
        insertErrorTableQuery = """insert into Claimant_Error 
                                    select s.*, '{}' from Claimant_stg s 
                                    where {} in 
                                        (select value from ColumnValueMappings
                                         where table_name = 'Claimant_stg' and column_name = '{}')"""
        cursor = self.__connection.cursor()
        
        for key, value in mapColsDict.items():
            cursor.execute(insertErrorTableQuery.format(value, key, key))
            if cursor.rowcount > 0:
                updateTableQuery = """
                                    update Claimant_stg 
                                    set {} = 
                                        (select mapped_value from ColumnValueMappings 
                                           where table_name = 'Claimant_stg' and column_name = '{}' 
                                           and value = Claimant_stg.{})
                                    where 
                                        exists 
                                            (select 1 from ColumnValueMappings
                                             where table_name = 'Claimant_stg' and column_name = '{}' 
                                                     and value = Claimant_stg.{})"""
                cursor.execute(updateTableQuery.format(key,key,key,key,key))
        return
    
    def __populateProvince(self):
        query = """update Claimant_stg 
                 set claimant_province = 
                     (select province from City_Province_Country 
                      where City_Province_Country.city = 
                                  Claimant_stg.claimant_city) 
                 where claimant_province is null or 
                 LTRIM(RTRIM(claimant_province)) = ''"""
        cursor = self.__connection.cursor()
        cursor.execute(query)
        query = """update Claimant_stg 
                 set claimant_province = 'Unknown' 
                 where claimant_province is null or 
                 LTRIM(RTRIM(claimant_province)) = ''"""
        cursor.execute(query)
        return
        
    def __cleanupNulls(self):
        nullColsDict = {
                    'claimant_name':['Null claimant_name',utils.populateUnknown],
                    'claimant_address':['Null claimant_address',utils.populateUnknown],
                    'claimant_city':['Null claimant_city',utils.populateUnknown],
                    'claimant_province':['Null claimant_province',self.__populateProvince],
                    'claimant_zip':['Null claimant_zip',utils.populateUnknown],
                   }       
        insertErrorTableQuery = "insert into Claimant_Error \
                                 select s.*, '{}'  from Claimant_stg s \
                                 where {} is null or LTRIM(RTRIM({})) = ''"
        
        cursor = self.__connection.cursor()
                                 
        for key, value in nullColsDict.items():
            cursor.execute(insertErrorTableQuery.format(value[0], key, key))
            if cursor.rowcount > 0:
                if key == "claimant_province":
                    value[1]();
                else :    
                    value[1](self.__connection, "Claimant_stg", key)
    
    def __validateCityProvince(self):
        
        errorTableInsertQuery = """
                                    insert into Claimant_Error 
                                     select s.*, 'Incorrect city-province' 
                                     from Claimant_stg s 
                                     where 
                                     not exists ( 
                                         select 1 from City_Province_Country r 
                                                where s.claimant_city = r.city and 
                                                      s.claimant_province = r.province)
                                """
        
        cursor = self.__connection.cursor()
        cursor.execute(errorTableInsertQuery)
    
        # update the records with correct province
        if cursor.rowcount > 0:
            updateProvinceQuery = """update Claimant_stg 
                                    set claimant_province = (select CASE 
                                                                    WHEN COUNT(province) > 1 
                                                                        THEN Claimant_stg.claimant_province 
                                                                    ELSE 
                                                                        MIN(province) 
                                                                   END 
                                                            from City_Province_Country r
                                                            where r.city = Claimant_stg.claimant_city)
                                    where 
                                    claimant_city in (select city from City_Province_Country) 
                                    and 
                                    not exists ( 
                                     select 1 from City_Province_Country r 
                                        where Claimant_stg.claimant_city = r.city and 
                                              Claimant_stg.claimant_province = r.province)"""
                                              
            cursor.execute(updateProvinceQuery)  
            # Validation check for remaining consistencies of city-province combination.
            getMismatchCityProvinceQuery = """
                                            select claimant_city, claimant_province 
                                            from Claimant_stg s 
                                            where 
                                            not exists ( 
                                                 select 1 from City_Province_Country r 
                                                 where s.claimant_city = r.city and 
                                                       s.claimant_province = r.province) 
                                            and (claimant_city is not null 
                                            and LTRIM(RTRIM(claimant_city)) != '')"""
            
            rows = cursor.execute(getMismatchCityProvinceQuery).fetchall()
            
            if rows:
                print "Following city-province pairs need manual attention. Rolling back the Claimant Dimension Load"
                for row in rows:
                    print "{}-{}".format(row.claimant_city, row.claimant_province)
                return False
        return True
    
    def __specialCleanup(self):
        '''Cleans up known issues with data'''
        errorTableInsertQuery = """insert into Claimant_Error 
                                     select s.*, 'Incorrect city-province' 
                                     from Claimant_stg s 
                                     where 
                                     s.claimant_city = 'New Jersey' and 
                                     s.claimant_province = 'New Yowk'"""
        cursor = self.__connection.cursor()
        cursor.execute(errorTableInsertQuery)
        
        updateQuery = """update Claimant_stg
                            set claimant_city = NULL,
                                claimant_province = NULL
                         where 
                             claimant_city = 'New Jersey' and     
                             claimant_province = 'New Yowk'"""
        cursor.execute(updateQuery)
        
    def __cleanup(self):
        self.__updateMappedValues()
        self.__specialCleanup()
        returnVal = self.__validateCityProvince()
        if not returnVal:
            return False
        self.__cleanupNulls()
        if not returnVal:
            return False
        return True
    
    def __loadLookup(self):
        insertLookupQuery = """
                                insert into Claimant_Lookup (claimant_prod_id, claimant_name)
                                select s.claimant_prod_id, s.claimant_name 
                                from Claimant_stg s LEFT JOIN Claimant_Lookup l
                                     ON (s.claimant_prod_id = l.claimant_prod_id and 
                                         s.claimant_name = l.claimant_name)
                                where l.claimant_key is null
                            """
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) ;
    
    def __loadDimension(self):
        insertDimensionQuery = """
                                insert into Claimant_Dimension 
                                select 
                                    l.claimant_key, 
                                    stg.claimant_prod_id,
                                    stg.claimant_name,
                                    stg.claimant_address,
                                    stg.claimant_city,
                                    stg.claimant_province,
                                    stg.claimant_zip    
                                from (
                                        Claimant_Lookup l JOIN Claimant_stg stg 
                                        ON (l.claimant_prod_id = stg.claimant_prod_id and
                                            l.claimant_name = stg.claimant_name)
                                     )
                                     LEFT JOIN Claimant_Dimension dim
                                     ON (dim.claimant_key = l.claimant_key)
                                where 
                                    dim.claimant_key is NULL 
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        print "{} rows inserted in Claimant_Dimension".format(cursor.rowcount)
                                
        