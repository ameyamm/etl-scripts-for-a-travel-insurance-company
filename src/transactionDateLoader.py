import utils

class TransactionDateLoader:
    '''
    Loads the transaction Date dimension
    '''
    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection
    
    def loadDimension(self):
        print "Loading Transaction Date Dimension"
        self.__loadStaging()
        self.__loadLookup()
        self.__loadDimension()
        self.__connection.commit()
        print "Load Complete"
        
    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Transaction_Date_stg");
        utils.truncateLookupTable(self.__connection, "Transaction_Date_Lookup", "transaction_date_key");
        utils.truncateTable(self.__connection, "Transaction_Date_Dimension");
        self.__connection.commit()
        print "\nClean Transaction Date and related staging tables"
        return
            
    def __loadStaging(self):
        utils.truncateTable(self.__connection, "Transaction_Date_stg");
        resetIdentityQuery = """
                                DBCC CHECKIDENT (Transaction_Date_stg, RESEED, 1)
                             """
        cursor = self.__connection.cursor()
        cursor.execute(resetIdentityQuery)
        
        insertStagingQuery = """
                                insert into Transaction_Date_stg
                                (
                                    date_readable_id,
                                    full_date,
                                    day_of_week,
                                    day_num_in_month,
                                    day_num_overall,
                                    day_name,
                                    day_abbrev,
                                    weekday_flag,
                                    week_num_in_year,
                                    week_num_overall,
                                    week_begin_date,
                                    week_begin_date_key,
                                    month,
                                    month_num_overall,
                                    month_name,
                                    month_abbrev,
                                    quarter,
                                    year,
                                    yearmo,
                                    fiscal_month,
                                    fiscal_quarter,
                                    fiscal_year,
                                    last_day_in_month_flag,
                                    same_day_year_ago
                                )
                                select 
                                    [date key],
                                    [full date],
                                    [day of week],
                                    [day num in month],
                                    [day num overall],
                                    [day name],
                                    [day abbrev],
                                    [weekday flag],
                                    [week num in year],
                                    [week num overall],
                                    [week begin date],
                                    [week begin date key],
                                    [month],
                                    [month num overall],
                                    [month name],
                                    [month abbrev],
                                    [quarter],
                                    [year],
                                    [yearmo],
                                    [fiscal month],
                                    [fiscal quarter],
                                    [fiscal year],
                                    [last day in month flag],
                                    [same day year ago]
                                from Date_Dimension
                                where [date key] is not NULL
                                order by [full date];
                             """
                                     
        cursor = self.__connection.cursor()
        cursor.execute(insertStagingQuery)        
    
    def __loadLookup(self):
        
        insertLookupQuery = """insert into Transaction_Date_Lookup (date_prod_id)
                                select s.date_prod_id 
                                from Transaction_Date_stg s LEFT JOIN Transaction_Date_Lookup l
                                     ON (s.date_prod_id = l.date_prod_id)
                                where l.transaction_date_key is NULL
                                order by s.date_prod_id"""
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) ;
        
    def __loadDimension(self):
        insertDimensionQuery = """
                                insert into Transaction_Date_Dimension 
                                (
                                    transaction_date_key,
                                    date_prod_id,
                                    date_readable_id,
                                    full_date,
                                    day_of_week,
                                    day_num_in_month,
                                    day_num_overall,
                                    day_name,
                                    day_abbrev,
                                    weekday_flag,
                                    week_num_in_year,
                                    week_num_overall,
                                    week_begin_date,
                                    week_begin_date_key,
                                    month,
                                    month_num_overall,
                                    month_name,
                                    month_abbrev,
                                    quarter,
                                    year,
                                    yearmo,
                                    fiscal_month,
                                    fiscal_quarter,
                                    fiscal_year,
                                    last_day_in_month_flag,
                                    same_day_year_ago
                                )   
                                select
                                    l.transaction_date_key,
                                    s.date_prod_id, 
                                    s.date_readable_id,
                                    s.full_date,
                                    s.day_of_week,
                                    s.day_num_in_month,
                                    s.day_num_overall,
                                    s.day_name,
                                    s.day_abbrev,
                                    s.weekday_flag,
                                    s.week_num_in_year,
                                    s.week_num_overall,
                                    s.week_begin_date,
                                    s.week_begin_date_key,
                                    s.month,
                                    s.month_num_overall,
                                    s.month_name,
                                    s.month_abbrev,
                                    s.quarter,
                                    s.year,
                                    s.yearmo,
                                    s.fiscal_month,
                                    s.fiscal_quarter,
                                    s.fiscal_year,
                                    s.last_day_in_month_flag,
                                    s.same_day_year_ago    
                                from (
                                        Transaction_Date_stg s JOIN Transaction_Date_Lookup l
                                        ON (s.date_prod_id = l.date_prod_id)
                                    ) LEFT JOIN Transaction_Date_Dimension dim 
                                      ON ( dim.transaction_date_key = l.transaction_date_key ) 
                                where dim.transaction_date_key is NULL
                                order by l.date_prod_id
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        print "{} rows inserted in Transaction_Date_Dimension".format(cursor.rowcount)
                                
        