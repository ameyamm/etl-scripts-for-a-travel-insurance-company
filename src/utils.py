ErrorValues = {
               'MISSING_DESC':'Missing Description',
               'MISSING_VAL':'Missing Value'
              }

def getErrorString(error_type):
    return ErrorValues.get(error_type)


'''
function to load lookup table
'''
def loadLookup(connection, sourceTable, lookupTable, sourceTableKey, dimensionKey):
    lookupInsertQuery="insert into {} ( {} ) \
                        select {} from {} ".format(lookupTable, sourceTableKey, sourceTableKey, sourceTable)   
                      
    cursor = connection.cursor()
    
    print "Loading {} table".format(lookupTable)
    
    cursor.execute(lookupInsertQuery) 
    if cursor.rowcount > 0:
        connection.commit() 
    else:
        print ("Failure in {} insert".format(lookupTable))
        connection.rollback()                     
# end of loadTransactionLookup

def populateUnknown(connection, table, column):
    """ Inserts value of UNKNOWN to String column of table. """
    query = "update {} set [{}] = 'Unknown' \
             where [{}] is null OR LTRIM(RTRIM([{}])) = ''".format(table, column, column, column);
    
    cursor = connection.cursor()
    cursor.execute(query);
    return 

def populateNA(connection, table, column):
    """ Inserts value of N.A. to String column of table. """
    query = "update {} set [{}] = 'Not Applicable' \
             where [{}] is null OR LTRIM(RTRIM([{}])) = ''".format(table, column, column, column);
    
    cursor = connection.cursor()
    cursor.execute(query);
    return 

def populateNegativeNumber(connection, table, column):
    """ Inserts value of -1 to number column of table. """
    query = "update {} set [{}] = -1 \
             where [{}] is null".format(table, column, column, column);
    
    cursor = connection.cursor()
    cursor.execute(query);
    return 

def populateZeroDate(connection,table,column):
    """ Inserts value of 0 to date column of table. """
    query = "update {} set {} = 0 \
             where {} is null".format(table, column, column, column);
    
    cursor = connection.cursor()
    cursor.execute(query);
    return 


def processNullCols(connection, tableName, nullColsDict):
    for col, value in nullColsDict.items():
        if value[1] > 0 and value[2] is not None:
            value[2](connection, tableName, col)
    return 

def getColsListForTable(connection, table):
    """ Returns the list of cols for a table """
    query = "SELECT COLUMN_NAME \
             FROM INFORMATION_SCHEMA.COLUMNS \
             WHERE TABLE_NAME = ?"
    
    listCols = [] 
    
    cursor = connection.cursor()        
    cursor.execute(query, table)
    
    rows = cursor.fetchall()
    
    for row in rows:
        listCols.append(row.COLUMN_NAME);
    
    return listCols

def truncateTable(connection, table):
    query = "delete from {}".format(table)
    cursor = connection.cursor()        
    cursor.execute(query)
    return            

def truncateLookupTable(connection, table, lookupColumn):
    query = "delete from {} where {} != 0".format(table, lookupColumn)
    cursor = connection.cursor()        
    cursor.execute(query)
    return            
