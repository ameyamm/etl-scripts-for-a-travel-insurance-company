import utils

class TransactionLoader:
    '''
    Loads the Transaction Dimension
    '''

    def __init__(self, connection):
        '''
        Constructor
        '''
        self.__connection=connection
    
    def loadDimension(self):
        print "Loading Transaction Dimension"
        self.__loadStagingTable()
        self.__loadLookup()
        self.__loadDimension()
        self.__connection.commit()
        print "Load Complete"
        
    def cleanDimension(self):
        utils.truncateTable(self.__connection, "Claim_Transaction_Type_stg");
        utils.truncateLookupTable(self.__connection, "Transaction_Lookup", "transaction_key");
        utils.truncateTable(self.__connection, "Transaction_Dimension");
        self.__connection.commit()
        print "\nClean Transaction and related staging tables"
        return
        
            
    def __loadStagingTable(self):
        utils.truncateTable(self.__connection, "Claim_Transaction_Type_stg");
        insertIntoStagingQuery = """
                                    insert into Claim_Transaction_Type_stg
                                    (
                                        claim_trans_prod_id,
                                        claim_trans_description,
                                        claim_trans_type
                                    ) 
                                    select
                                    claim_trans_key,
                                    claim_trans_description,
                                    claim_trans_type
                                    from Claim_Transaction_Type 
                                    where claim_trans_key is not NULL
                                    order by claim_trans_key ;
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertIntoStagingQuery) ;
         
    def __loadLookup(self):
        insertLookupQuery = """insert into Transaction_Lookup (claim_trans_prod_id)
                                select s.claim_trans_prod_id 
                                from Claim_Transaction_Type_stg s LEFT JOIN Transaction_Lookup l
                                        ON (s.claim_trans_prod_id = l.claim_trans_prod_id)
                                where l.transaction_key is null
                            """
        
        cursor = self.__connection.cursor()
        cursor.execute(insertLookupQuery) ;
    
    def __loadDimension(self):
        insertDimensionQuery = """
                                insert into Transaction_Dimension 
                                select l.transaction_key, s.* 
                                from (
                                        Transaction_Lookup l JOIN Claim_Transaction_Type_stg s 
                                            ON (l.claim_trans_prod_id = s.claim_trans_prod_id)
                                     ) 
                                     LEFT JOIN Transaction_Dimension dim
                                     ON (l.transaction_key = dim.transaction_key)
                                where dim.transaction_key is null
                                """
        cursor = self.__connection.cursor()
        cursor.execute(insertDimensionQuery)
        print "{} rows inserted in Transaction_Dimension".format(cursor.rowcount)
                                
        